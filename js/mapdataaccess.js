/**
 * Created by doug on 5/7/17.
 */


mapDataAccess = function () {
    var self = this;
    var _clientPage;
    var _mapPointArray = undefined;

    function initialize () {
        console.log('mapDataAccess, init: ...');
        buildMapPointArray();
    }

    /**
     * Function to create a singe or multi-dimention array
     *
     * Examples:
     *  createArray(3, 2)
     *  createArray(2)
     *
     * @param length
     * @returns {any[]}
     */
    function createArray(length) {
        console.log('mapDataAccess, createArray: ... Num args = ' + arguments.length);
        var arr = new Array(length || 0),
            i = length;

        if (arguments.length > 1) {
            var args = Array.prototype.slice.call(arguments, 1);
            while(i--) arr[length-1 - i] = createArray.apply(this, args);
        }

        return arr;
    }

    /*
    function getDataJson() {
        var dataArray = [
            {name: 'Home',description: 'Medford home address', street: '2971 Spring Hills Dr',city: 'Medford', state: 'OR',zip: '97504'},
            {name: 'Holiday RV Park', description: 'Best park in the Medford area',street: '201 Fern Valley Road',city: 'Phoenix',state: 'OR',zip: '97535'},
            {name: 'Donahue Park', description: 'Small park for day use', street: '1687 Spring St',city: 'Medford', state: 'OR',zip: '97504'}
        ];

        console.log('mapDataAccess, getDataArray: Returning Address DB...' + JSON.stringify(dataArray));

        return dataArray;
    }
    */

    function getMapPointArray() {
        return _mapPointArray;
    }


    // Builds the lat/long array from the street address DataArray
    function buildMapPointArray() {
        data = parkingDataAccess.getMapData();
        console.log('mapDataAccess, buildMapPointArray: Got Address DB...' + JSON.stringify(data));

        var numRecordDataElements = 4;
        _mapPointArray = createArray(data.length, numRecordDataElements);

        var x=0;
        for (x=0; x<data.length; x++) {
            _mapPointArray[x][0] = data[x].street + ', ' + data[x].city + ', ' + data[x].state;
            _mapPointArray[x][1] = data[x].description;
            _mapPointArray[x][2] = data[x].name;
            _mapPointArray[x][3] = 'available';
        }
    }

    /****************************************************************
     * Class Factory and UI mapping
     ****************************************************************/
    return {
        init: function (page) {
            _clientPage = page;

            initialize();
        },
        getMapPointArray: getMapPointArray
    };
}();
