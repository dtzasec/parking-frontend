/**
 * mapsupport.js
 */



mapSupport = function () {
	var _apiKey = "AIzaSyA1ed_J1t9Wy2dkuIpwk1oqNIE0OiDqz_Y";
	//var _apiKey = "AIzaSyCpoJj2B-XcaSim0jlzEVJ9NMAoSXpY3gg";
	//var _apiKey = "AIzaSyC4Ko7ZXX5t0ACaSl25AqF2lWjUN-o0-Bk";			// 2/18/18

	var _clientPage;
	var _destAddress;		// Use as center of the map
	var _centerLatlng = {lat: -25.363, lng: 131.044};
	var _mapZoomLevel = 17;
	var _map;				// 'global'/class wide map instance
	
	//var _lat;
	//var _long;

    function log(msg) {
        console.log('' + msg);
    }

	
	function initMapSimple(refAddress) {
		// Init _map and center on the last searched address
		if (!!_destAddress) {
			log("initMapSimple: _destAddress initialized...");
		} else {
			log("initMapSimple: Invalid _destAddress, init with default...");
			_destAddress = "548 Market St, San Francisco, Ca";
		}

        mapSupport.geocodeAddress(_destAddress)
			.done (function (res) {
                log("initMapSimple: Geocode Address complete... " + JSON.stringify(res));
				var myOptions = {
					zoom: _mapZoomLevel,
					center: new google.maps.LatLng(res.point.lat, res.point.long),
					mapTypeId: 'roadmap',
					zoomControl: true
				};
				_map = new google.maps.Map($('#map')[0], myOptions);

				// Center the map on the most recent search address
				processSubmitAddress();
				log("initMapSimple: Map initialization complete...");

			})
			.fail(function (res) {

			});

		/*
		geocodeAddress(_destAddress, function(status, point) {
			if (status) {
                var myOptions = {
                    zoom: _mapZoomLevel,
                    center: new google.maps.LatLng(point.lat, point.long),
                    mapTypeId: 'roadmap',
                    zoomControl: true
                };
                _map = new google.maps.Map($('#map')[0], myOptions);

                // Center the map on the most recent search address
                processSubmitAddress();
                log("initMapSimple: Map initialization complete...");
            } else {
                log("initMapSimple: Geocode Address failed!");
			}
	    });
	    */

		/*
		marker.addListener('click', function() {
		    map.setZoom(8);
		    map.setCenter(marker.getPosition());
		  });
		  */
	}
	
	/**
	 * processMapPoint()
	 * 
	 * Places a point on the map.
	 *
	 * point = {lat: 789789, long: 324345}
	 */
	function processMapPoint(point) {
		log("processMapPoint: [" + point.lat + ", " + point.long + "]");
		
		var myLatLng = {lat: point.lat, lng: point.long};
		
		_map = new google.maps.Map(document.getElementById('map'), {
		    center: myLatLng,
		    zoom: _mapZoomLevel
		  });
		
		/*
		var marker = new google.maps.Marker({
	          position: myLatLng,
	          map: _map,
	          label: 'P',
	          title: _destAddress
	        });
	        */
		
		getGeoPointList();
	}


	
	/**
	 * getGeoPointList()
	 * 
	 * Temporary test function to return a fixed set of points to place on the map.  They should
	 * be around my home address.
	 *
	 * This function returns the Parking Space data list.  This is loaded into the Map, the data is loaded every time a
	 * new Map is created.
	 * 
	 * http://geojson.org/geojson-spec.html#geometry-collection
	 * http://stackoverflow.com/questions/28596237/loading-a-geojson-object-directly-into-google-maps-v3
	 */
	function getGeoPointList() {
		log("getGeoPointList: ...");
		/*
		var pointlist = [{ "type": "Point", "coordinates": [37.796656, -121.969411] }, 
		                 { "type": "Point", "coordinates": [37.795778, -121.969383] }]; 
		*/
		
		var pointlist = {
			    "type": "FeatureCollection",
			    "features": [
			      {
			        "type": "Feature",
			        "properties": {
			        	 label: 'P',
				          title: _destAddress
			        },
			        "geometry": {
			          "type": "MultiPoint",
			          "coordinates": [
			              [37.796656, -121.969411], [37.795778, -121.969383]
			            
			          ]
			        }
			      }
			    ]
			  };

		
		_map.data.addGeoJson(pointlist);
	}
	
	
	/**
	 * multiPointExample()
	 * 
	 * This works.  It plots the addresses in the array.
	 * http://stackoverflow.com/questions/3059044/google-maps-js-api-v3-simple-multiple-marker-example
	 * 
	 *  TODO: Add "hover" control
	 *  TODO: Add list to the list side bar
	 */
	/*
	function multiPointExample() {
		   var addr2 = mapDataAccess.getMapPointArray();
*/
			   /*[
	    	                 ['1647 Harlan Dr, Danville, Ca','Single space','available'],
	    	                 ['1660 Harlan Dr, Danville, Ca','Double Space','full'],
	    	                 ['1657 Harlan Dr, Danville, Ca','Street in front of house','available'],
	    	                 ['1670 Harlan Dr, Danville, Ca','Garage bay 1 on left', 'full']
    	                 ];
    	                 */
/*
    	    log("multiPointExample: Number of entries = " + addr2.length);
    	    //console.log("multiPointExample: Address #3 = " + addr2[1][0]);
    	    
    	    for (var x = 0; x < addr2.length; x++) {
    	    	var markerAddress = "";
    	    	markerAddress =	addr2[x][0];
    	    	var markerPlace = addr2[x][1];
                var markerDescription = addr2[x][2];
    	    	var markerStatus = addr2[x][3];
    	    	
    	    	log("multiPointExample: Address #" + x + " = " + markerAddress);
    	        $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+markerAddress+'&sensor=false', null, function (data) {
    	            var p = data.results[0].geometry.location
    	            var latlng = new google.maps.LatLng(p.lat, p.lng);
    	            
    	            // Create a new Marker for each Address entry in the Database
    	            var marker = new google.maps.Marker({
    	                position: latlng,
    	                map: _map,
    	                label: 'P',
    	  	          	title: markerAddress
    	            });

					// Add Events to each Marker created
    	            marker.addListener('click', function() {
    	            	var infowindow = new google.maps.InfoWindow({
                            content: markerPlace + ' - ' + markerDescription
                        });
    	            	// Center on the Marker
    	                _map.setZoom(18);
    	                _map.setCenter(marker.getPosition());

                        infowindow.open(_map, marker);
                        log('multiPointExample: Marker on map clicked... ');
    	                
    	                //infowindow.setContent(markerPlace);
    	                //infowindow.open(_map, marker);
    	              });

    	        });
    	    }
    	
	}
*/

	var _numRecords;
	var _currentRecord;
	var _mapRecordArray;
	function transferMapDataToGoogle() {
        log('transferMapDataToGoogle: ...');
        _mapRecordArray = mapDataAccess.getMapPointArray();
        _numRecords = _mapRecordArray.length;
        _currentRecord = 0;

        log('transferMapDataToGoogle: Sending first record; _currentRecord = ' +_currentRecord+ ', _numRecords = ' +_numRecords);

        // Start the record transfer loop
        transferMapDataRecord(_mapRecordArray[_currentRecord], transferNextMapDataRecord);
	}

	function transferNextMapDataRecord () {
        log('transferNextMapDataRecord: ...');
		_currentRecord++;

		// Continue looping through the records until all the records are sent
		if (_currentRecord <= (_numRecords - 1)) {
            log('transferNextMapDataRecord: Sending next record; _currentRecord = ' +_currentRecord+ ', _numRecords = ' +_numRecords);
            transferMapDataRecord(_mapRecordArray[_currentRecord], transferNextMapDataRecord);
		}
	}


	function transferMapDataRecord (record, callback) {

        log("transferMapDataRecord: Record = " + record);

        if (!!record) {
            var markerAddress = record[0];
            var markerPlace = record[1];
            var markerDescription = record[2];
            var markerStatus = record[3];

            log("transferMapDataRecord: Address = " + markerAddress);

            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + markerAddress + '&sensor=false', function (data) {

                log('transferMapDataRecord: Transfer to Google complete, adding marker...');

                var p = data.results[0].geometry.location
                var latlng = new google.maps.LatLng(p.lat, p.lng);

                // Create a new Marker for each Address entry in the Database
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: _map,
                    label: 'P',
                    title: markerAddress,
					description: markerDescription
                });


                // Add Events to each Marker created
                marker.addListener('click', function () {
                    var infowindow = new google.maps.InfoWindow({
                        content: markerPlace + ' - ' + markerDescription
                    });
                    // Center on the Marker
                    _map.setZoom(18);
                    _map.setCenter(marker.getPosition());

                    infowindow.open(_map, marker);
                    log('transferMapDataRecord: Marker on map clicked... ');

                    $(_clientPage).find("#address").val(marker.title);
                    $(_clientPage).find("#description").val(marker.description);

                    //infowindow.setContent(markerPlace);
                    //infowindow.open(_map, marker);
                });

                callback();
            });
        } else {
            log('transferMapDataRecord: ERROR: Invalid record!');
		}
	}

	/*
	function setMapCenter_Address(address) {
        log('setMapCenter_Address: ...');
		// Look up address geocode
		geocodeAddress(address, function(status, point){
			if (status) {
                // Center the map
                _map.setCenter({lat: point.lat, lng: point.long});
                //_map.setCenter({lat:point.lat, lng:point.long});

                addMarker_Full(point, "A", address);
            } else {
				log('setMapCenter_Address:  Geocode Address failed! ');
			}
		});
	}
	*/
    function setMapCenter_Address(address) {
        log('setMapCenter_Address: ...');
        // Look up address geocode
        geocodeAddress(address)
			.done(function(res) {
                log('setMapCenter_Address:  Geocode Address completed... ');
				// Center the map
				_map.setCenter({lat: res.point.lat, lng: res.point.long});
				//_map.setCenter({lat:point.lat, lng:point.long});

				addMarker_Full(res.point, "A", address);

            })
			.fail(function(res) {
                log('setMapCenter_Address:  Geocode Address failed! ');
			});
    }

	function setMapCenter_Point(point) {
		_map.setCenter({lat:point.lat, lng:point.long});
	}

	function addMarker_Bare (p) {
        log('addMarker_Bare: ...');
		var latlng = new google.maps.LatLng(p.lat, p.long);

		// Create a new Marker for each Address entry in the Database
		var marker = new google.maps.Marker({
			position: latlng,
			map: _map,
			animation: google.maps.Animation.DROP,
			title: "Marker"
		});

		/*
		 ,
		 place: p.lat + ", " + p.long
		 */

		marker.addListener('click', function() {
			var infowindow = new google.maps.InfoWindow();
			// Center on the Marker
			_map.setZoom(18);
			_map.setCenter(marker.getPosition());
		});
	}

	function addMarker_Full(p, label, title) {
        log('addMarker_Full: ...');
		var latlng = new google.maps.LatLng(p.lat, p.long);

		// Create a new Marker for each Address entry in the Database
		var marker = new google.maps.Marker({
			position: latlng,
			map: _map,
			label: label,
			animation: google.maps.Animation.DROP,
			title: title
		});

		/*
		 ,
		 place: p.lat + ", " + p.long
		 */

		// Add Events to each Marker created
		marker.addListener('click', function() {
			log('addMarker_Full: ...');
			var infowindow = new google.maps.InfoWindow({
				content: label
			});
			// Center on the Marker
			_map.setZoom(18);
			_map.setCenter(marker.getPosition());


			//infowindow.setContent(markerPlace);
			//infowindow.open(_map, marker);
		});

		setMarkerGreen(marker);
	}

	function setMarkerGreen(marker) {
		marker.setIcon('https://maps.google.com/mapfiles/ms/icons/green-dot.png');
	}

	/**
	 * geocodeAddress()
	 * 
	 * POSTs a street address to Google and retrieves the lattitude and longitude of that
	 * address so it can be put on the map. 
	 */
	function geocodeAddress(address) {
		var deferred = $.Deferred();

		log("geocodeAddress: Getting lat/long for address = " + address);
		_destAddress = address;
		
		var postUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=" + address;
		
		$.ajax({
			type: "POST",
			url: postUrl,
			//data: JSON.stringify(params),
			async: true,
			//contentType: 'application/json',
			//dataType: json,
			cache: false,
			success: function(data) {
				log("geocodeAddress: ... Google Maps request status =  " + data.status);
				log("geocodeAddress: ... Googla Maps data rcvd =  " + JSON.stringify(data));

				if (data.status != 'OVER_QUERY_LIMIT') {
                    _lat = data.results[0].geometry.location.lat;
                    _long = data.results[0].geometry.location.lng;

                    var point = {
                        lat: _lat,
                        long: _long
                    };

                    _centerLatlng.lat = _lat;
                    _centerLatlng.lng = _long;

                    //processMapPoint(_lat, _long);

                    //callback(true, point);
                    deferred.resolve({status: true, point: point});
                } else {
                    log("geocodeAddress: ... Error: " + JSON.stringify(data));
					//callback(false, null);
                    deferred.reject({status: false});
				}
			},
			error: function(xhr) {
				log("geocodeAddress: ... Error: " + xhr.responseText);
				//callback(false, null);
                deferred.reject({status: false});
			} 
		});

		return deferred.promise();
	}


	function processSubmitAddress () {
		log("processSubmitAddress: ...");

		_destAddress = $("#destaddress").val();
        writeLocalStorage();		// Save new reference address
		log("processSubmitAddress: destAddress = " + _destAddress);
		setMapCenter_Address(_destAddress);

		//geocodeAddress(_destAddress);
		// Load all the Parking Spot (database) into the Map instance
		transferMapDataToGoogle();
	}

	function setCurrentLocation() {
		log("setCurrentLocation: ...");
		getLocation();
	}

	function getLocation() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(showPosition);
		} else {
			x.innerHTML = "Geolocation is not supported by this browser.";
		}
	}

	function showPosition(position) {
		var pt = {
			lat: position.coords.latitude,
			long: position.coords.longitude
		};

		log("showPosition: Lat = " + position.coords.latitude);
		log("showPosition: Long = " + position.coords.longitude);

		$(_clientPage).find("#latPosition").val(position.coords.latitude);
		$(_clientPage).find("#longPosition").val(position.coords.longitude);

		setMapCenter_Point(pt);
		addMarker_Full(pt, 'C', "Current Location");
	}

	/****************************************************************
	 * Local Storage
	 ****************************************************************/
	function writeLocalStorage() {
		log("writeLocalStorage: Setting _destAddress to... " + _destAddress);
		localStorage.setItem("destaddress", _destAddress);
	}
	
	function readLocalStorage() {
		_destAddress = localStorage.getItem("destaddress");
		log("readLocalStorage: _destAddress = " + _destAddress);
	}
	
	/****************************************************************
	 * Class Factory and UI mapping
	 ****************************************************************/
	return {
		init: function (page) {
			_clientPage = page;

            //  Initialize helper classes
            mapDataAccess.init(_clientPage);
			
			// Init the UI 
			readLocalStorage();

			log("init: Setting UI text to... " + _destAddress);
			//$("#_clientPage.destaddress").val(_destAddress);
			$(_clientPage).find("#destaddress").val(_destAddress);

			// Set the map center to the last entered address
			initMapSimple(_destAddress);
		},
		initMapSimple: initMapSimple,
		geocodeAddress: geocodeAddress,
		processSubmitAddress: processSubmitAddress,
		setCurrentLocation: setCurrentLocation
	};
}();

