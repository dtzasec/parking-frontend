/**
 * New node file
 */

zasecsupport = function() {
	var _clientPage;
	//var _urladdress = 'http://localhost:8080';
	var _urladdress = 'http://zasec.hopto.org:8080';
	
	
	function processLoginForm() {
		console.log("processLoginForm: ...");
		
		var fullurl = _urladdress + '/BasicRESTSvc/v1/LoginUser';
		
		$.ajax({
	        url: fullurl,
	        dataType: 'text',
	        type: 'post',
	        contentType: 'application/json',
	        data: JSON.stringify({
	        		"command": 'LoginUser',
					"username": $('#email').val(),
					"password": $('#pwd').val()
			}),
	        success: function( data, textStatus, jQxhr ){
	        	var resStr = JSON.stringify(data);
	        	console.log("JSON recieved.. " + resStr);
	        	var result = JSON.parse(data);
	        	console.log("POST succeeded... " + result.userkey);
	        	
	        	if (result.status == "SUCCESS") {
	        		console.log("Login successful");
	        		localStorage.setItem("userkey", result.userkey);
	        		
	        		alert("Login successful");
	        		if ($('#remember').prop('checked')) {
	        			console.log("Remember un/pw");
	        			localStorage.setItem("email", $('#email').val());
	        			//localStorage.setItem("passwd", $('#pwd').val());
	        			localStorage.setItem("saveinput", 1);
	        		} else {
	        			console.log("Forget un/pw");
	        			// NOTE: KEEP THE EMAIL??, needed for Device operations ??
	        			// NOTE:  Must remove them from local storage
	        			localStorage.removeItem("email");
	        			//localStorage.removeItem("passwd");
	        			localStorage.setItem("saveinput", 0);
	        		}
	        	} else {
	        		alert("Login failed: " + result.status);
	        	}
	        	$('#response pre').html( data );
	        },
	        error: function( jqXhr, textStatus, errorThrown ){
	        	//console.log("Error: " + testStatus);
	        	console.log( "The error thrown is... " + errorThrown );
	        	alert("Login did not complete, try again");
	        }
	    });

	}
	
	/****************************************************************
	 * Class Factory
	 ****************************************************************/
	return {
		init: function(page) {
			_clientPage = page;
		},
		processLoginForm: processLoginForm
	};
	
}();
