/**
 * New node file
 */

loginui = function() {
	function initLoginUi() {
		if (localStorage["email"]) {
            $('#email').val(localStorage["email"]);
        }
        if (localStorage["password"]) {
            $('#pwd').val(localStorage["password"]);
        }
        if (localStorage["saveinput"] == 1) {
            $('#remember').prop('checked', true);
        }
	}
	
	/****************************************************************
	 * Class Factory
	 ****************************************************************/
	return {
		init: function(page) {
			_clientPage = page;
			
			$(_clientPage).find('#btnLoginUiInit').click(function(evt) {
				initLoginUi();
			});
		},
		initLoginUi: initLoginUi
	};
}();