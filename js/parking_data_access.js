/**
 * Created by doug on 6/4/17.
 */

parkingDataAccess = function () {
    function initialize() {

    }

    function log (msg) {
        console.log('parkingDataAccess, ');
    }

    function addParkingSpot (data) {
        // Send the form data to the Backend for storage
    }

    function getParkingData() {
        // Retrieve the parking data list from the Backend
        // ... ALL spots to be entered into the Google Map
    }

    function getDataJson() {
        var dataArray = [
            {name: 'Home',description: 'Medford home address', street: '2971 Spring Hills Dr',city: 'Medford', state: 'OR',zip: '97504'},
            {name: 'Holiday RV Park', description: 'Best park in the Medford area',street: '201 Fern Valley Road',city: 'Phoenix',state: 'OR',zip: '97535'},
            {name: 'Donahue Park', description: 'Small park for day use', street: '1687 Spring St',city: 'Medford', state: 'OR',zip: '97504'}
        ];

        log('getDataArray: Returning Address DB...' + JSON.stringify(dataArray));

        return dataArray;
    }

    /****************************************************************
     * Class Factory and UI mapping
     ****************************************************************/
    return {
        init: function (page) {
            _clientPage = page;

            mongoUtility.init(_clientPage);

            initialize();

            log("init: Data Access library initialized... ");

            //$(_clientPage).find("#destaddress").val(_destAddress);
        },
        getMapData: getDataJson
    };
}();